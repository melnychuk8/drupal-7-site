/**
 * @file
 * Defines gulp tasks to be run by Gulp task runner.
 */
/* global require */
(function (r) {
    "use strict";
    var gulp = r('gulp'),
        sass = r('gulp-sass'),
        watch = r('gulp-watch'),
        notify = r("gulp-notify") ,
        prefixer = r('gulp-autoprefixer'),
        sourcemaps = r('gulp-sourcemaps'),
        cssmin = r('gulp-clean-css');

    // Init Gulp
    gulp.task('init', function() {
        'use strict';
        gulp.src('scss/**/*.scss')
            .pipe(sourcemaps.init())
            .pipe(sass({
                sourceMap: true,
                errLogToConsole: true
            }).on('error', function(err) {
                notify().write(err);
                //this.emit('end');
            }))
            .pipe(prefixer())
            .pipe(cssmin())
            .pipe(sourcemaps.write())
            .pipe(gulp.dest('css/'));
    });
    // Watch task.
    gulp.task('watch', function() {
        'use strict';
        gulp.watch('scss/**/*.scss', ['init']);
    });

    // Compile sass into CSS & auto-inject into browsers.
    gulp.task('scss', function() {
        'use strict';
        return gulp.src("scss/*.scss").pipe(sass()).pipe(gulp.dest("css"))
    });
}(require));