(function ($) {
    Drupal.behaviors.dru7 = {
        attach: function (context, settings) {

            // Fixed related block when scroll.
            $('#block-views-related-article-block', context).once('dru7', function () {
                var element = $('#block-views-related-article-block'),
                    originalY = element.offset().top;

                // Space between element and top of screen (when scrolling).
                var topMargin = 40;

                $(window).on('scroll', function (event) {
                    var scrollTop = $(window).scrollTop();
                    var scrollElement = scrollTop - originalY + topMargin;

                    if (window.innerWidth < 768) {
                        element.removeAttr('style');
                        return null;
                    } else {
                        element.stop(false, false).animate({
                            top: scrollTop < originalY ? 0 : scrollElement
                        }, 400);
                    }
                });
            });
        }

    };
}(jQuery));
